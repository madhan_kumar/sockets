const express = require("express");
const app = express();
const http = require("http").Server(app);
var io = require('socket.io')(http);
var port = 4000;

var counts =0;
io.on('connection', function(socket){
  counts++;
  // socket.emit("connectclientstobroadcast",{"count":counts,"mssg":"Peoples connected"})
  socket.emit('newclientconnect',{ description: 'Hey, welcome!'});
  io.sockets.emit('connectclientstobroadcast',{ "count": counts ,"mssg": ' clients connected!'})
  console.log('a user connected');
  
  socket.on("chat message",function(message){
  	 	io.emit('chat message', message);
  })

  	socket.on('privatechatroom',function(data){
		socket.join(data.email);
		socket.emit('response',{mes:"you are added"});
    });

    socket.on('leaveprivateroom',function(email){
    	socket.leave(email);
    	socket.emit('leavedroom',true);
    })

    socket.on('sendmail',function(data)
	{
	    io.sockets.in(data.email).emit('new_msg', {msg: data.message,email:data.email});
	            console.log(data.email);
	});
	
	  socket.on('disconnect', function () {
	  counts--;
	  io.sockets.emit("connectclientstobroadcast",{"count":counts,"mssg":"Peoples connected"});
   });
});

http.listen(port);
console.log("localhost:"+port);
